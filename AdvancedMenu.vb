﻿Sub InsertMenuItems()
    'проверка есть ли уже текущее меню
    'If Application.CustomMenus.Item(1).Label = "Advanced" Then 'already have new menu
    '    Exit Sub
    'End If
    
    Dim objMenu As HMIMenu
    Dim objMenuItem As HMIMenuItem
    
    '''''''''''''''''''''''''''
    'Подменю корневое >
    '''''''''''''''''''''''''''
    Set objMenu = Application.CustomMenus.InsertMenu(1, "AdvancedMenu_1", "Advanced")
    '
    '''''''''''''''''''''''''''
    'Меню Изменить текст св-ва
    '''''''''''''''''''''''''''
    Set objMenuItem = objMenu.MenuItems.InsertMenuItem(1, "ReplaceMenu_1", "Изменить текст св-ва")
    objMenuItem.ShortCut = "Ctrl+R"
    objMenuItem.Macro = "ChangePropertyText"
    '
    '''''''''''''''''''''''''''
    'Меню Свойства
    '''''''''''''''''''''''''''
    '
    Set objMenuItem = objMenu.MenuItems.InsertMenuItem(2, "Properties_1", "Свойства")
    objMenuItem.ShortCut = "Alt+X"
    objMenuItem.Macro = "MyShowPropertiesDialog"
    '
    '''''''''''''''''''''''''''
    'Меню Выбор тега
    '''''''''''''''''''''''''''
    '
    Set objMenuItem = objMenu.MenuItems.InsertMenuItem(3, "TagSelect_1", "Выбор тега")
    objMenuItem.ShortCut = "Ctrl+T"
    objMenuItem.Macro = "MyTagSelect"
    '
    '''''''''''''''''''''''''''
    'Подменю File >
    '''''''''''''''''''''''''''
    Set objSubMenu = objMenu.MenuItems.InsertSubMenu(4, "File_1", "Файл")
    '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(5, "SaveAll_1", "Сохранить всё")
        objMenuItem.ShortCut = "Ctrl+Shift+S"
        objMenuItem.Macro = "MySaveAll"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(6, "CloseAll_1", "Закрыть всё")
        objMenuItem.ShortCut = "Ctrl+Shift+W"
        objMenuItem.Macro = "MyCloseAll"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(7, "CloseActive_1", "Закрыть")
        objMenuItem.ShortCut = "Ctrl+W"
        objMenuItem.Macro = "MyCloseActive"
        '
    
    '
    '''''''''''''''''''''''''''
    'Меню Скопировать имя объекта
    '''''''''''''''''''''''''''
    '
    Set objMenuItem = objMenu.MenuItems.InsertMenuItem(8, "CopyName_1", "Скопировать имя объекта")
    objMenuItem.ShortCut = "Ctrl+Shift+C"
    objMenuItem.Macro = "MyCopyObjectName"
    
    '''''''''''''''''''''''''''
    'Подменю Геометрия >
    '''''''''''''''''''''''''''
    Set objSubMenu = objMenu.MenuItems.InsertSubMenu(9, "Geometry_1", "Геометрия")
    '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(10, "Square_1", "Сделать стороны равными")
        objMenuItem.ShortCut = "Alt+S"
        objMenuItem.Macro = "MySquare"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(11, "WidthPlus_1", "Ширина +1")
        objMenuItem.ShortCut = "Ctrl+1"
        objMenuItem.Macro = "WidthPlus"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(12, "WidthMinus_1", "Ширина -1")
        objMenuItem.ShortCut = "Ctrl+2"
        objMenuItem.Macro = "WidthMinus"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(13, "HeightPlus_2", "Высота +1")
        objMenuItem.ShortCut = "Ctrl+3"
        objMenuItem.Macro = "HeightPlus"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(14, "HeightMinus_2", "Высота -1")
        objMenuItem.ShortCut = "Ctrl+4"
        objMenuItem.Macro = "HeightMinus"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(15, "SwapSides_1", "Поменять местами Ш и В")
        objMenuItem.ShortCut = "Ctrl+5"
        objMenuItem.Macro = "SwapSides"
        '
    '''''''''''''''''''''''''''
    'Подменю Шрифт >
    '''''''''''''''''''''''''''
    Set objSubMenu = objMenu.MenuItems.InsertSubMenu(16, "Font_1", "Шрифт")
    '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(17, "DecrFont_1", "-1pt шрифт")
        objMenuItem.ShortCut = "Ctrl+L"
        objMenuItem.Macro = "DecreaseFont"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(18, "IncrFont_1", "+1pt шрифт")
        objMenuItem.ShortCut = "Ctrl+B"
        objMenuItem.Macro = "IncreaseFont"
        '
        
    '''''''''''''''''''''''''''
    'Подменю Позиционирование >
    '''''''''''''''''''''''''''
    Set objSubMenu = objMenu.MenuItems.InsertSubMenu(19, "Position_1", "Позиционирование")
    '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(20, "CenterVert", "Центрировать на экране верт.")
        objMenuItem.Macro = "MyCenterScreenVert"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(21, "CenterHor", "Центрировать на экране гориз.")
        objMenuItem.Macro = "MyCenterScreenHor"
        '
        
    '''''''''''''''''''''''''''
    'Подменю Слои >
    '''''''''''''''''''''''''''
    Set objSubMenu = objMenu.MenuItems.InsertSubMenu(22, "Layer_1", "Слои")
    '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(23, "Layer_0", "Layer 0 - Подложка")
        objMenuItem.Macro = "SetLayer_0"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(24, "Layer_2", "Layer 2 - Трубы")
        objMenuItem.Macro = "SetLayer_2"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(25, "Layer_4", "Layer 4 - Датчики/Арматура")
        objMenuItem.Macro = "SetLayer_4"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(26, "Layer_6", "Layer 6 - Индикаторы/Табло")
        objMenuItem.Macro = "SetLayer_6"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(27, "Layer_8", "Layer 8 - Подписи")
        objMenuItem.Macro = "SetLayer_8"
        '
             
    '''''''''''''''''''''''''''
    'Меню Подготовить картинку
    '''''''''''''''''''''''''''
    Set objMenuItem = objMenu.MenuItems.InsertMenuItem(28, "PrepareMenu_1", "Подготовить картинку (уст. Ш, В, GlobalScheme)")
    objMenuItem.Macro = "PreparePic"
    '
    '''''''''''''''''''''''''''
    'Меню Отключить GlobalScheme
    '''''''''''''''''''''''''''
    Set objMenuItem = objMenu.MenuItems.InsertMenuItem(29, "GlobalSchemeOff_1", "Отключить GlobalScheme")
    objMenuItem.Macro = "GlobalSchemeOff"
    
    '
    '''''''''''''''''''''''''''
    'Меню Сделать объект прозрачным
    '''''''''''''''''''''''''''
    Set objMenuItem = objMenu.MenuItems.InsertMenuItem(30, "TranspObj_1", "Сделать объект прозрачным")
    objMenuItem.Macro = "SetTransp"	
    '''''''''''''''''''''''''''
    'Подменю Объекты IO >
    '''''''''''''''''''''''''''
    Set objSubMenu = objMenu.MenuItems.InsertSubMenu(31, "IO_1", "Объекты IO")
    '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(32, "IO_2", "Сделать объект вывода")
        objMenuItem.Macro = "MakeOutputField"
        '
        Set objMenuItem = objSubMenu.SubMenu.InsertMenuItem(33, "IO_3", "Сделать объект ввода")
        objMenuItem.Macro = "MakeInputField"
    '
    '''''''''''''''''''''''''''
    'Меню редактировать TagPrefix
    '''''''''''''''''''''''''''
    Set objMenuItem = objMenu.MenuItems.InsertMenuItem(34, "EditTP_1", "Редактировать TagPrefix")
    objMenuItem.ShortCut = "Alt+P"
    objMenuItem.Macro = "EditTP"
    '''''''''''''''''''''''''''
End Sub

Sub EditTP()
    Dim objObject
    Dim currentTP, newTP
    
    If (ActiveDocument.Selection.Count >= 1) Then
        currentTP = ActiveDocument.Selection(1).Properties("TagPrefix").value
        Debug.Print ("TP = " & currentTP)
    Else
        Exit Sub
    End If
    
    newTP = InputBox("Новый Tag Prefix", "Замена Tag Prefix", currentTP)
    
    For Each objObject In ActiveDocument.Selection
        objObject.TagPrefix = newTP
    Next objObject
End Sub

Sub MakeOutputField()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.FillStyle = 65536
        objObject.BorderWidth = 1
        objObject.GlobalColorScheme = False
        objObject.Operation = False
        objObject.BoxType = 0
        objObject.FONTSIZE = 14
        objObject.AlignmentTop = 1
    Next objObject
End Sub

Sub MakeInputField()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.BackColor = RGB(255, 255, 255)
        objObject.FillStyle = 0
        objObject.Operation = True
        objObject.BorderWidth = 1
        objObject.GlobalColorScheme = False
        objObject.BoxType = 2
        objObject.FONTSIZE = 14
        objObject.AlignmentTop = 1
    Next objObject
End Sub

Sub SetTransp()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.FillStyle = 65536
        objObject.BorderWidth = 0
        objObject.GlobalColorScheme = False
    Next objObject
End Sub

Sub SetLayer_8()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.layer = 8
    Next objObject
End Sub

Sub SetLayer_6()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.layer = 6
    Next objObject
End Sub

Sub SetLayer_4()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.layer = 4
    Next objObject
End Sub

Sub SetLayer_2()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.layer = 2
    Next objObject
End Sub

Sub SetLayer_0()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.layer = 0
    Next objObject
End Sub

Sub GlobalSchemeOff()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.GlobalColorScheme = False
    Next objObject
End Sub

Sub PreparePic()
    ActiveDocument.Width = 1920 'нужная ширина окна
    ActiveDocument.Height = 800 'нужная высота окна
    ActiveDocument.GlobalColorScheme = False
    
    ActiveDocument.Layers(1).Name = "Подложка"
    ActiveDocument.Layers(3).Name = "Трубы"
    ActiveDocument.Layers(5).Name = "Датчики/Арматура"
    ActiveDocument.Layers(7).Name = "Индикаторы/Табло"
    ActiveDocument.Layers(9).Name = "Подписи"
End Sub

Sub SwapSides()
    Dim objObject, heigth_, width_
    For Each objObject In ActiveDocument.Selection
        height_ = objObject.Height
        width_ = objObject.Width
        objObject.Height = width_
        objObject.Width = height_
    Next objObject
End Sub

Sub HeightMinus()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.Height = objObject.Height - 1
    Next objObject
End Sub

Sub HeightPlus()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.Height = objObject.Height + 1
    Next objObject
End Sub

Sub WidthMinus()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.Width = objObject.Width - 1
    Next objObject
End Sub

Sub WidthPlus()
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        objObject.Width = objObject.Width + 1
    Next objObject
End Sub

Sub MyCenterScreenHor() 'center object on screen horizontally
    Dim objObject, objHeigth, objWeigth, screenWeigth, screenHeigth
    screenWeigth = ActiveDocument.Width
    screenHeigth = ActiveDocument.Height
        For Each Our_Object In ActiveDocument.Selection
            objHeigth = Our_Object.Height
            objWeigth = Our_Object.Width
            Our_Object.Left = (screenWeigth \ 2) - (objWeigth \ 2)
        Next Our_Object
End Sub

Sub MyCenterScreenVert() 'center object on screen vertically
    Dim objObject, objHeigth, objWeigth, screenWeigth, screenHeigth
    screenWeigth = ActiveDocument.Width
    screenHeigth = ActiveDocument.Height
        For Each Our_Object In ActiveDocument.Selection
            objHeigth = Our_Object.Height
            objWeigth = Our_Object.Width
            Our_Object.Top = (screenHeigth \ 2) - (objHeigth \ 2)
        Next Our_Object
End Sub

Sub DecreaseFont() '-1 pt to font size
    Dim PropertyName
    PropertyName = "FontSize"
    Dim objObject
       For Each Our_Object In ActiveDocument.Selection
        For Each Each_Property In Our_Object.Properties
            If Each_Property.Name = PropertyName Then
                    Our_Object.Properties(PropertyName).value = _
                    Our_Object.Properties(PropertyName).value - 1
            End If
        Next Each_Property
    Next Our_Object
End Sub

Sub IncreaseFont() '+1 pt to font size
    Dim PropertyName
    PropertyName = "FontSize"
    Dim objObject
       For Each Our_Object In ActiveDocument.Selection
        For Each Each_Property In Our_Object.Properties
            If Each_Property.Name = PropertyName Then
                    Our_Object.Properties(PropertyName).value = _
                    Our_Object.Properties(PropertyName).value + 1
            End If
        Next Each_Property
    Next Our_Object
End Sub

Sub MySquare() 'make squared object
    Dim Side
    Dim objObject
    For Each objObject In ActiveDocument.Selection
        Side = (objObject.Width + objObject.Height) \ 2
        objObject.Width = Side
        objObject.Height = Side
    Next objObject
End Sub

Sub MyCopyObjectName() 'copy selected object name to clipboard
    Dim clipboard As Object
    Set clipboard = CreateObject("new:{1C3B4210-F441-11CE-B9EA-00AA006B1A69}")
    clipboard.SetText ActiveDocument.Selection.Item(1).ObjectName
    clipboard.PutInClipboard
End Sub

Sub MyTagSelect() 'show tag selection dialog
    ActiveDocument.Application.ShowTagDialog
End Sub

Sub MyShowPropertiesDialog() 'show properties dialog
    ActiveDocument.Selection.Application.ShowPropertiesDialog
End Sub

Sub MySaveAll() 'save all opened pictures
    Documents.SaveAll
End Sub

Sub MyCloseAll() 'close all opened pictures
    Documents.CloseAll
End Sub

Sub MyCloseActive() 'close active picture
    ActiveDocument.Close
End Sub

Sub ChangePropertyText() 'change text of entered property on active screen
    Dim Our_Object As HMIObject
    Dim Each_Property As HMIProperty
    Dim VarName As String
    
    Dim SearchText As String, ReplaceText As String, PropertyName As String
    PropertyName = InputBox("PropertyName?", "Replace", "Text")
    SearchText = InputBox("Search for ...?", "Replace")
    ReplaceText = InputBox("Replace with ...?", "Replace")
    Dim TextValue As String
    
    For Each Our_Object In ActiveDocument.HMIObjects
        For Each Each_Property In Our_Object.Properties
            If Each_Property.Name = PropertyName Then
                TextValue = Our_Object.Properties(PropertyName).value
                If (TextValue = SearchText) Then
                    Debug.Print TextValue & " -> " & ReplaceText & vbCrLf
                    Our_Object.Properties(PropertyName).value = ReplaceText
                End If
            End If
        Next Each_Property
    Next Our_Object
End Sub